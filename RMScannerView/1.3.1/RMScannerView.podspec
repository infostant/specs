Pod::Spec.new do |s|
  s.name         = 'RMScannerView'
  s.version      = '1.3.1'
  s.summary      = 'Simple barcode scanner UIView subclass for iOS apps. Quickly and efficiently scans a large variety of barcodes using the iOS device\'s built in camera.'
  s.author       = {'iRareMedia' => 'contact@iraremedia.com'}
  s.license      = 'MIT'
  s.homepage 	   = 'https://github.com/iRareMedia/RMScannerView'
  s.source       = {:git => "https://github.com/iRareMedia/RMScannerView", :commit =>'fb0214570dad39e7540542028df75e7ade01cbc4'}
  s.platform     = :ios, '5.0'
  s.source_files = '*.{h,m}'
  s.resources    = '*.png'
  s.frameworks	 = 'Foundation', 'UIKit','AVFoundation'
  s.requires_arc = true
end
