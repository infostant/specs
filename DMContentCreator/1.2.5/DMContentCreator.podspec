Pod::Spec.new do |s|
  s.name     = 'DMContentCreator'
  s.version  = '1.2.5'
  s.license  = 'Commercial'
  s.summary  = 'Content creator plugin for DMConnex plugin'
  s.homepage = 'http://www.infostant.com/'
  s.author   = { 'Nattawut Singhchai' => 'wut@2bsimple.com' }

  s.source   = { :git => 'https://infostant@bitbucket.org/infostant/dmcontentcreator.git',:tag => '1.2.5'}

  s.platform = :ios
  s.ios.deployment_target = '7.0'
  s.source_files = 'DMContentCreator/Classes/*.{h,m}','DMContentCreator/Classes/**/*.{h,m}'

  s.resources = 'DMContentCreator/Resources/*.{storyboard,png,bundle}'
  #s.exclude_files = 'Graphics/Default-568h@2x.png'
  s.requires_arc = true
  s.library = 'sqlite3'
  s.frameworks = 'MapKit'
    s.dependency 'UIImage-Resize'
    s.dependency 'LAUtilities','0.0.1'
    s.dependency 'BlocksKit'
    s.dependency 'ALActionBlocks'
    s.dependency 'WTGlyphFontSet'
    s.dependency 'WTGlyphFontSet/fontawesome'
    s.dependency 'HCYoutubeParser'
    s.dependency 'FoundationExtension'
    s.dependency 'RNGridMenu'
    s.dependency 'MBProgressHUD'
    s.dependency 'iOS7Colors'
    s.dependency 'DMPhotoPicker'
    s.dependency 'DMReorderTableView'
    s.dependency 'AFNetworking'
    s.dependency 'FXBlurView'
    s.dependency 'WYPopoverController'
end
