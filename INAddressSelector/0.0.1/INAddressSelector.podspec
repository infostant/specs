Pod::Spec.new do |s|
  s.name     = 'INAddressSelector'
  s.version  = '0.0.1'
  s.license  = 'MIT'
  s.summary  = 'SQLite queryer for iOS'
  s.homepage = 'http://www.infostant.com/'
  s.author   = { 'Nattawut Singhchai' => 'wut@2bsimple.com' }

  s.source   = { :git => 'https://infostant@bitbucket.org/infostant/INAddressSelector',:tag => "#{s.version}"}

  s.platform = :ios
  s.ios.deployment_target = '6.0'
  s.source_files = 'INAddressSelector/INAddressSelector/*.{h,m}'
  s.resources = 'INAddressSelector/resources/*.sqlite'
  #s.exclude_files = 'Graphics/Default-568h@2x.png'
  s.requires_arc = true
  s.dependency 'INSQLiteHelper'
end
